require("colors");
const socket = require('ws');
const ws = new socket('ws://tkbip.test:8081');

ws.on('open', function open() {
    console.log('connected'.green.bold);
    ws.send("local service connected...");
});

ws.on('close', function close() {
    console.log('disconnected'.red.bold);
});

ws.on('message', function incoming(data) {
    console.log(`Roundtrip time: ${Date.now() - data} ms`);

    setTimeout(function timeout() {
        ws.send(Date.now());
    }, 500);
});
