#!/usr/bin/env node
const socket = require('ws');
const wss = new socket.Server({
    port: 8010
});

wss.on('connection', (ws) => {
    ws.on('message', (data) => {
        wss.clients.forEach(function each(client) {
            if (client.readyState === socket.OPEN) {
                client.send(data);
            }
        });
    });
});
